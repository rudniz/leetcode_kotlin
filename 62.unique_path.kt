class Unique {

    private lateinit var memo: Array<IntArray>
    
    fun uniquePaths(m: Int, n: Int): Int {
        val data = Array(m) { IntArray(n) }
        for (i in 0 until m) data[i][0] = 1
        for (i in 1 until n) data[0][i] = 1
        for (i in 1 until m) {
            for (j in 1 until n) {
                data[i][j] = data[i - 1][j] + data[i][j - 1]
            }
        }
        return data[m - 1][n - 1]
    }

    fun uniquePathsDPBottomUp(m: Int, n: Int): Int {
        val curr = IntArray(n) { 1 }
        var prev = IntArray(n) { 1 }
        for (i in 1 until m) {
            for (j in 1 until n) {
                curr[j] = curr[j - 1] + prev[j]
            }
            prev = curr
        }
        return curr[n - 1]
    }

    fun uniquePathsDPBottomUp_2(m: Int, n: Int): Int {
        val curr = IntArray(n) { 1 }
        for (i in 1 until m) {
            for (j in 1 until n) {
                curr[j] += curr[j - 1]
            }
        }
        return curr[n - 1]
    }

    fun uniquePathsRecursionTopDown(m: Int, n: Int): Int {
        memo = Array(m) { IntArray(n) { 0 } }
        for (i in 0 until m) memo[i][0] = 1
        for (j in 0 until n) memo[0][j] = 1
        return backtracking(m - 1, n - 1)
    }

    private fun backtracking(i: Int, j: Int): Int {
        if (memo[i][j] != 0) return memo[i][j]
        else memo[i][j] = backtracking(i - 1, j) + backtracking(i, j - 1)
        return memo[i][j]
    }
}

fun main(args: Array<String>) {
    val unique = Unique()
    val tests = arrayOf(
        intArrayOf(3, 7),
        intArrayOf(3, 2),
        intArrayOf(3, 3),
        intArrayOf(5, 1),
        intArrayOf(1, 1)
    )
    val expected = intArrayOf(28, 3, 6, 1, 1)
    for (i in 0 until tests.size) {
        val result = unique.uniquePathsRecursionTopDown(tests[i][0], tests[i][1])
        if (result != expected[i]) {
            println("result is $result but expected is ${expected[i]}")
        } else println("OK")
    }
}