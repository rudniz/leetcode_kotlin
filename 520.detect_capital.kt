class Capital {
    fun detectCapitalUse(word: String): Boolean {
        return word.substring(1) == word.substring(1).toLowerCase()
                || word == word.toUpperCase()
    }
}

fun main() {
    val capital = Capital()
    val tests = arrayOf("USA", "FlaG", "notcapital", "notCapital", "FFFFFFFFFFFFFFFFFFFFf")
    val expected = arrayOf(true, false, true, false, false)
    for (i in tests.indices) {
        val result = capital.detectCapitalUse(tests[i])
        if (result != expected[i]) println("result is $result but expected is ${expected[i]}")
        else println("OK")
    }
}