class ReverseInteger {
    fun reverseUsingSubstring(x: Int): Int {
        val negative = x < 0
        val s = if (negative) x.toString().substring(1) else x.toString()
        val res =
            try {
                s.reversed().toInt()
            } catch (e: Exception) {
                0
            }
        return if (negative) -res else res
    }

    fun reverse(x: Int): Int {
        var original = x
        var result = 0
        var previousResult = 0
        while (original != 0) {
            result = result * 10 + original % 10
            if ((result - original % 10) / 10 != previousResult) return 0
            previousResult = result
            original = original / 10
        }
        return result
    }
}

fun main(args: Array<String>) {
    val reverseIneger = ReverseInteger()
    val tests = intArrayOf(-123, -1234, 0, 1234567890, 1234567899, -1234567890)
    val expected = intArrayOf(-321, -4321, 0, 987654321, 0, 0)
    for (i in 0 until tests.size - 1) {
        val result = reverseIneger.reverseUsingSubstring(tests[i])
        if (result != expected[i]) {
            println("result is $result but expected is ${expected[i]}")
        } else println("OK")
    }
}