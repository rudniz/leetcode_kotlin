class ToeplitzMatrix {
    fun isToeplitzMatrix(matrix: Array<IntArray>): Boolean {
        for (i in 1 until matrix.size) {
            for (j in 1 until matrix[i].size) {
                if (matrix[i][j] != matrix[i - 1][j - 1]) return false
            }
        }
        return true
    }
}

fun main(args: Array<String>) {
    val toeplitzMatrix = ToeplitzMatrix()
    val tests = arrayOf(
        arrayOf(intArrayOf(1, 2, 3, 4), intArrayOf(5, 1, 2, 3), intArrayOf(9, 5, 1, 2)),
        arrayOf(intArrayOf(1, 2), intArrayOf(2, 2))
    )
    val expected = arrayOf(true, false)
    for (i in 0 until tests.size) {
        val result = toeplitzMatrix.isToeplitzMatrix(tests[i])
        if (result != expected[i]) println("result is $result but expected is ${expected[i]}")
        else println("OK")
    }
}