class Search {
    fun searchInsert(nums: IntArray, target: Int): Int {
        var start = 0
        var end = nums.size - 1
        while (start <= end) {
            val mid = (start + end) / 2
            if (nums[mid] == target) return mid
            if (nums[mid] < target) start = mid + 1
            else end = mid - 1
        }
        return start
    }
}

fun main(args: Array<String>) {
    val search = Search()
    val tests = intArrayOf(1, 3, 5, 6)
    val targets = intArrayOf(0, 2, 7, 4)
    val expected = intArrayOf(0, 1, 4, 2)
    for (i in 0 until targets.size) {
        val result = search.searchInsert(tests, targets[i])
        if (result != expected[i]) {
            println("result is $result but expected is ${expected[i]}")
        } else println("OK")
    }
}