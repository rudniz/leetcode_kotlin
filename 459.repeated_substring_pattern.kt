class RepeatedPattern {

    fun repeatedSubstringPattern(s: String): Boolean {
        for (i in 1..s.length / 2) {
            if (s.length % i == 0) {
                if (checkSubstring(s, i)) {
                    return true
                }
            }
        }
        return false
    }

    private fun checkSubstring(s: String, i: Int): Boolean {
        for (start in i until s.length step i) {
            if (s.substring(0, i) != s.substring(start, start + i)) {
                return false
            }
        }
        return true
    }

    fun repeatedSubstringPattern2(s: String): Boolean {
        return s in (s + s).substring(1, (s + s).length - 1)
    }
}


fun main(args: Array<String>) {
    val repeatedPattern = RepeatedPattern()
    val tests = arrayOf("aaa", "a", "abab", "abcabcabcabcabcd", "ababa", "abcabcabcabcabc", "ab")
    val expected = arrayOf(true, false, true, false, false, true, false)
    for (i in 0 until tests.size) {
        val result = repeatedPattern.repeatedSubstringPattern2(tests[i])
        if (result != expected[i]) {
            println("result is $result but expected is ${expected[i]}")
        } else {
            println("OK")
        }
    }
}

