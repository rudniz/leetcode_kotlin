/**
 * Example:
 * var li = ListNode(5)
 * var v = li.`val`
 * Definition for singly-linked list.
 * class ListNode(var `val`: Int) {
 *     var next: ListNode? = null
 * }
 */

class AddTwoNumbers {
    fun addTwoNumbers(l1: ListNode?, l2: ListNode?): ListNode? {
        val li = ListNode(0)
        var p = l1
        var q = l2
        var current = li
        var carry = 0
        while (p != null || q != null) {
            val valueP = if (p != null) p.`val` else 0
            val valueQ = if (q != null) q.`val` else 0
            val digit = (valueP + valueQ + carry) % 10
            carry = (valueP + valueQ + carry) / 10

            current.next = ListNode(digit)
            current = current.next!!
            p = p?.next
            q = q?.next
        }
        if (carry != 0) {
            current.next = ListNode(1)
        }
        return li.next
    }

    fun printTree(l: ListNode?) {
        var traverse = l
        while (traverse != null) {
            print(traverse.`val`)
            traverse = traverse.next
        }
    }

    fun createTree(nums: Array<Int>): ListNode {
        val l = ListNode(nums[0])
        var traverse = l
        for (i in 1 until nums.size) {
            traverse.next = ListNode(nums[i])
            traverse = traverse.next!!
        }
        return l
    }

    fun compareTree(l1: ListNode?, l2: ListNode?): Boolean {
        var traverse1 = l1
        var traverse2 = l2
        while (traverse1 != null) {
            if (traverse2 == null) {
                return false
            }
            if (traverse1.`val` != traverse2.`val`) {
                return false
            }
            traverse1 = traverse1.next
            traverse2 = traverse2.next
        }
        return traverse2 == null
    }
}

fun main(args: Array<String>) {
    val add = AddTwoNumbers()
    val l1 = arrayOf(
        add.createTree(arrayOf(1, 2, 3)),
        add.createTree(arrayOf(2, 2, 3, 6, 7)),
        add.createTree(arrayOf(0)),
        add.createTree(arrayOf(1, 0, 0, 0))
    )
    val l2 = arrayOf(
        add.createTree(arrayOf(1, 2, 3)),
        add.createTree(arrayOf(1, 2, 8)),
        add.createTree(arrayOf(0)),
        add.createTree(arrayOf(9, 9, 9, 9))
    )
    val expected = arrayOf(
        add.createTree(arrayOf(2, 4, 6)),
        add.createTree(arrayOf(3, 4, 1, 7, 7)),
        add.createTree(arrayOf(0)),
        add.createTree(arrayOf(0, 0, 0, 0, 1))
    )

    for (i in 0 until l1.size) {
        val res = add.addTwoNumbers(l1[i], l2[i])
        if (add.compareTree(res, expected[i]) != true) {
            print("result is ")
            add.printTree(res)
            println()
            print("but expected is ")
            add.printTree(expected[i])
            println()
        } else println("OK")
    }
}