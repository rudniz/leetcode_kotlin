class Altitude {
    fun largestAltitude(gain: IntArray): Int {
        var highest = 0
        var currentElevation = 0
        for (g in gain) {
            currentElevation += g
            highest = maxOf(currentElevation, highest)
        }
        return highest
    }
}

fun main(args: Array<String>) {
    val altitude = Altitude()
    val tests = arrayOf(
        intArrayOf(-5, 1, 5, 0, -7),
        intArrayOf(-4, -3, -2, -1, 4, 3, 2)
    )
    val expected = intArrayOf(1, 0)
    for (i in 0 until tests.size) {
        val result = altitude.largestAltitude(tests[i])
        if (result != expected[i]) {
            println("result is $result but expected is ${expected[i]}")
        } else println("OK")
    }
}