class PivotIndex {
    fun pivotIndex(nums: IntArray): Int {
        var rightSum = nums.sum() - nums[0]
        var leftSum = 0
        var current = 0
        while (current < nums.size - 1) {
            if (rightSum == leftSum) {
                return current
            } else {
                leftSum += nums[current]
                rightSum -= nums[current + 1]
                current += 1
            }
        }
        if (rightSum == leftSum && current == nums.size - 1) return current
        return -1
    }

    fun pivotIndex2(nums: IntArray): Int {
        var leftSum = 0
        val sum = nums.sum()
        for (i in 0 until nums.size) {
            if (leftSum == sum - leftSum - nums[i]) {
                return i
            } else leftSum += nums[i]
        }
        return -1
    }
}

fun main(args: Array<String>) {
    val p = PivotIndex()
    val tests = arrayOf(
        intArrayOf(1, 7, 3, 6, 5, 6),
        intArrayOf(1, 2, 3),
        intArrayOf(2, 1, -1),
        intArrayOf(-1, -1, 0, 1, 1, 0)
    )
    val expected = intArrayOf(3, -1, 0, 5)

    for (i in 0 until tests.size) {
        val result = p.pivotIndex2(tests[i])
        if (result != expected[i]) {
            println("result is $result but expected is ${expected[i]}")
        } else println("OK")
    }
}