class PrimeBitsCount {
    private val primes = setOf(2, 3, 5, 7, 11, 13, 17, 19)
    fun countPrimeSetBits(left: Int, right: Int): Int {
        var result = 0
        for (i in left..right) {
            if (countBits(i) in primes) result++
        }
        return result
    }

    private fun countBits(i: Int): Int {
        var bitCount = 0
        var n = i
        while (n > 0) {
            bitCount += n and 1
            n = n shr 1
        }
        return bitCount
    }

    private fun countBitsWithString(i: Int): Int {
        return Integer.toBinaryString(i).filter { it == '1' }.count()
    }
}

fun main(args: Array<String>) {
    val primeBitsCount = PrimeBitsCount()
    val tests = arrayOf(
        intArrayOf(6, 10),
        intArrayOf(10, 15)
    )
    val expected = intArrayOf(4, 5)

    for (i in tests.indices) {
        val result = primeBitsCount.countPrimeSetBits(tests[i][0], tests[i][1])
        if (result != expected[i]) {
            println("expected is ${expected[i]} but result is ")
        } else println("OK")
    }
}