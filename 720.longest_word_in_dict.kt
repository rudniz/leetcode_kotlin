class LongestWord {
    fun longestWord(words: Array<String>): String {
        words.sort()
        var res = ""
        val d = mutableMapOf<String, String>()
        for (i in words) {
            if (i.length == 1 || d.contains(i.substring(0, i.length - 1))) {
                d[i] = i
                if (i.length > res.length) res = i
            }
        }
        return res
    }

    fun longestWord2(words: Array<String>): String {
        words.sort()
        val set = HashSet<String>()
        var result = ""
        set.add("")
        for (w in words) {
            if (set.contains(w.substring(0, w.length - 1))) {
                set.add(w)
                if (w.length > result.length) result = w
            }
        }
        return result
    }
}

fun main(args: Array<String>) {
    val l = LongestWord()
    val tests = arrayOf(
        arrayOf("app", "a", "apply", "ap", "appl", "apple", "b", "ban"),
        arrayOf("a", "banana", "b"),
        arrayOf("w", "wo", "wor", "worl", "world"),
        arrayOf("m", "mo", "moc", "moch", "mocha", "l", "la", "lat", "latt", "latte", "c", "ca", "cat")
    )
    val expected = arrayOf("apple", "a", "world", "latte")
    for (i in 0 until tests.size) {
        val result = l.longestWord2(tests[i])
        if (result != expected[i]) {
            println("result is $result but expected is ${expected[i]}")
        } else println("OK")
    }
}