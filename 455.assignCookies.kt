class AssignCookies {
    fun findContentChildren(g: IntArray, s: IntArray): Int {
        g.sort()
        s.sort()
        var child = 0
        var cookie = 0
        var result = 0
        while (child < g.size && cookie < s.size) {
            if (g[child] > s[cookie]) {
                cookie += 1
            } else {
                result += 1
                cookie += 1
                child += 1
            }
        }
        return result
    }
}

fun main(args: Array<String>) {
    val assign = AssignCookies()
    val children = arrayOf(
            intArrayOf(5, 4, 1, 1, 3, 1),
            intArrayOf(1, 2, 3),
            intArrayOf(5, 6, 4)
    )
    val cookies = arrayOf(
            intArrayOf(6, 1),
            intArrayOf(1, 2, 3),
            intArrayOf(1, 1, 3, 2)
    )

    val expectedResults = intArrayOf(2, 3, 0)

    for (i in 0..2) {
        val result = assign.findContentChildren(children[i], cookies[i])
        val expected = expectedResults[i]
        if (result != expected) {
            println("result is $result but expected is $expected")
        } else println("OK")
    }
}