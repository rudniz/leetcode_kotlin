class KeyboardRow {
    fun findWords(words: Array<String>): Array<String> {
        val result = arrayListOf<String>()
        val first = "qwertyuiop".toSet()
        val second = "asdfghjkl".toSet()
        val third = "zxcvbnm".toSet()
        for (w in words) {
            val lower = w.toLowerCase()
            if (lower.all { c: Char -> first.contains(c) }
                    || lower.all { c: Char -> second.contains(c) }
                    || lower.all { c: Char -> third.contains(c) }) {
                result.add(w)
            }
        }
        return result.toTypedArray()
    }

    fun findWords2(words: Array<String>): Array<String> {
        val result = arrayListOf<String>()
        val hash = HashMap<Char, Int>()
        val listOfRows = listOf("qwertyuiop", "asdfghjkl", "zxcvbnm")
        for (i in 0..2) {
            for (c in listOfRows[i]) {
                hash.put(c, i)
            }
        }
        for (w in words) {
            val lower = w.toLowerCase()
            var index = hash.get(lower[0])
            for (c in lower) {
                if (hash.get(c) != index) {
                    index = -1
                    break
                }
            }
            if (index != -1) {
                result.add(w)
            }
        }
        return result.toTypedArray()
    }
}

fun main(args: Array<String>) {
    val k = KeyboardRow()
    val tests = arrayOf("qwe", "qaw", "asd", "A", "Qw")
    val expected = arrayOf("qwe", "asd", "A", "Qw")
    val result = k.findWords2(tests)
    if (result.contentEquals(expected)) {
        print("OK")
    } else {
        print("result is " + result.contentToString() + "but expected is " + expected.contentToString())
    }
}