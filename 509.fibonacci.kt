class Fibonacci {
    val map = hashMapOf(0 to 0, 1 to 1)
    fun fib(n: Int): Int {
        return calculateFibonacci(n)
    }

    private fun calculateFibonacci(n: Int): Int {
        if (n !in map) {
            map[n] = calculateFibonacci(n - 1) + calculateFibonacci(n - 2)
        }
        return map[n]!!
    }

    fun calculateFibonacciIterative(n: Int): Int {
        if (n <= 1) return n
        var first = 0
        var second = 1
        for (i in 2..n) {
            val result = first + second
            first = second
            second = result
        }
        return second
    }
}

fun main() {
    val fibonacci = Fibonacci()
    val tests = intArrayOf(0, 1, 2, 3, 4, 15)
    val expected = intArrayOf(0, 1, 1, 2, 3, 610)
    for (i in tests.indices) {
        val result = fibonacci.fib(tests[i])
        if (result != expected[i]) println("Recursive: expected is ${expected[i]}")
        else println("Recursive: OK")

        val resultIterative = fibonacci.calculateFibonacciIterative(tests[i])
        if (resultIterative != expected[i]) println("Iterative: expected is ${expected[i]}")
        else println("Iterative: OK")
    }
}