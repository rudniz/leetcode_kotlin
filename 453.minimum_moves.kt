class MinimumMoves {
    fun minMoves(nums: IntArray): Int {
        var minMoves = 0
        val minimum = nums.min()!!
        for (i in nums) {
            minMoves += i - minimum
        }
        return minMoves
    }

    fun minMoves2(nums: IntArray): Int {
        val minimum = nums.min()!!
        return (nums.map { it - minimum }).sum()
    }

    fun minMoves3(nums: IntArray): Int {
        return nums.sum() - nums.size * nums.min()!!
    }
}

fun main(args: Array<String>) {
    val solution = MinimumMoves()
    val test = intArrayOf(1, 2, 3, 4)
    val expected = 6
    val result = solution.minMoves3(test)
    if (solution.minMoves2(test) != expected) {
        println("result is $result but expected is $expected")
    } else println("OK")
}
