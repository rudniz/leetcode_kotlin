class RemoveDuplicates {
    fun removeDuplicates(nums: IntArray): IntArray {
        var i = 0
        var j = 1
        while (j < nums.size) {
            if (j < nums.size && nums[j] == nums[j - 1]) {
                j++
            } else {
                i++
                nums[i] = nums[j]
                j++
            }
        }
        return nums.slice(0..i).toIntArray()
    }

    fun removeDuplicates2(nums: IntArray): IntArray {
        var i = 0
        for (j in 1 until nums.size) {
            if (nums[i] != nums[j]) {
                i++
                nums[i] = nums[j]
            }
        }
        return nums.slice(0..i).toIntArray()
    }
}

fun main(args: Array<String>) {
    val remove = RemoveDuplicates()
    val test = arrayOf(
        intArrayOf(0, 1, 1, 2, 2, 3),
        intArrayOf(1, 2, 3),
        intArrayOf(1, 1, 1, 1),
        intArrayOf(1, 1, 2, 3, 3)
    )

    val expected = arrayOf(
        intArrayOf(0, 1, 2, 3),
        intArrayOf(1, 2, 3),
        intArrayOf(1),
        intArrayOf(1, 2, 3)
    )
    for (i in 0 until test.size) {
        val result = remove.removeDuplicates2(test[i])
        if (!result.contentEquals(expected[i])) {
            println("result is ${result.joinToString()} but expected is ${expected[i].joinToString()}")
        } else println("OK")
    }
}