class MaximumSubarray {
    fun maxSubArray(nums: IntArray): Int {
        var maxTotal = nums[0]
        var maxLocal = 0
        for (i in 0 until nums.size) {
            maxLocal += nums[i]
            maxTotal = Integer.max(maxLocal, maxTotal)
            if (maxLocal < 0) maxLocal = 0
        }
        return maxTotal
    }
}

fun main(args: Array<String>) {
    val maximumSubarray = MaximumSubarray()
    val tests = arrayOf(
        intArrayOf(-2, 1, -3, 4, -1, 2, 1, -5, 4),
        intArrayOf(1),
        intArrayOf(-1),
        intArrayOf(-2, -1)
    )
//    val expected = intArrayOf(-1)

    val expected = intArrayOf(6, 1, -1, -1)
    for (i in 0 until tests.size) {
        val result = maximumSubarray.maxSubArray(tests[i])
        if (result != expected[i]) {
            println("expected is ${expected[i]} but result is $result")
        } else println("OK")
    }
}