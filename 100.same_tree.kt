/**
 * Example:
 * var ti = TreeNode(5)
 * var v = ti.`val`
 * Definition for a binary tree node.
 * class TreeNode(var `val`: Int) {
 *     var left: TreeNode? = null
 *     var right: TreeNode? = null
 * }
 */
class SameTree {
    fun isSameTree(p: TreeNode?, q: TreeNode?): Boolean {
        if (p == null && q == null) return true
        if (p == null || q == null) return false
        if (p.`val` == q.`val`) {
            return (isSameTree(p.left, q.left) && isSameTree(p.right, q.right))
        }
        return false
    }

}

fun main() {
    val sameTree = SameTree()
    val tree = TreeNode(1)
    tree.left = TreeNode(0)
    tree.right = TreeNode(2)
    val duplicateTree = TreeNode(1)
    duplicateTree.left = TreeNode(0)
    duplicateTree.right = TreeNode(2)
    val anotherTree = TreeNode(1)
    anotherTree.left = TreeNode(2)
    anotherTree.right = TreeNode(0)
    val expected = arrayOf(true, false)
    val result = arrayListOf<Boolean>()
    result.add(sameTree.isSameTree(tree, duplicateTree))
    result.add(sameTree.isSameTree(tree, anotherTree))
    for (i in expected.indices) {
        if (expected[i] != result[i]) println("result is ${result[i]} but expected is ${expected[i]}")
        else println("OK")
    }
}