class LongestPrefix {
    fun longestCommonPrefix(strs: Array<String>): String {
        if (strs.size == 0) return ""
        val min = strs.min()
        val max = strs.max()
        for ((i, c) in min!!.withIndex()) {
            if (max!![i] != c) {
                return min.substring(0, i)
            }
        }
        return min
    }

    fun longestCommonPrefix2(strs: Array<String>): String {
        if (strs.isEmpty()) return ""
        var prefix = strs[0]
        for (i in strs) {
            while (i.indexOf(prefix) != 0) {
                prefix = prefix.substring(0, prefix.length - 1)
            }
        }
        return prefix
    }
}

fun main(args: Array<String>) {
    val prefix = LongestPrefix()
    val tests = arrayOf(
        arrayOf("flu", "flower", "flight"),
        arrayOf("a", "ab", "aba"),
        arrayOf("", ""),
        arrayOf("", "a"),
        arrayOf("a", "b", "c")
    )
    val expected = arrayOf("fl", "a", "", "", "")
    for ((i, word) in tests.withIndex()) {
        val result = prefix.longestCommonPrefix2(word)
        if (result != expected[i]) {
            println("result is $result but expected is ${expected[i]}")
        } else println("OK")
    }
}