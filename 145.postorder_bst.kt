import java.util.*
import kotlin.collections.ArrayList

/**
 * Example:
 * var ti = TreeNode(5)
 * var v = ti.`val`
 * Definition for a binary tree node.
 * class TreeNode(var `val`: Int) {
 *     var left: TreeNode? = null
 *     var right: TreeNode? = null
 * }
 */
class PostOrder {
    val result = ArrayList<Int>()
    fun postorderTraversal(root: TreeNode?): List<Int> {
        traverseTree(root)
        return result
    }

    private fun traverseTree(root: TreeNode?) {
        if (root == null) return
        traverseTree(root.left)
        traverseTree(root.right)
        result.add(root.`val`)
    }

    fun traverseTreeIterative(root: TreeNode?): List<Int> {
        val resultIterative = ArrayList<Int>()
        val stack = ArrayDeque<TreeNode?>()
        if (root == null) return result
        var curr = root
        stack.push(curr)
        while (!stack.isEmpty()) {
            curr = stack.pop()
            resultIterative.add(curr!!.`val`)

            if (curr.left != null) {
                stack.push(curr.left)
            }
            if (curr.right != null) {
                stack.push(curr.right)
            }
        }
        return resultIterative.reversed()
    }
}

fun createBinarySearchTree(list: List<Int>): TreeNode? {
    if (list.isEmpty()) return null
    val mid = list.size / 2
    val root = TreeNode(list[mid])
    root.left = createBinarySearchTree(list.subList(0, mid))
    root.right = createBinarySearchTree(list.subList(mid + 1, list.size))
    return root
}

fun main() {
    val postOrder = PostOrder()
    val test = listOf(1, 2, 3, 4, 5, 6, 7)
    val expected = intArrayOf(1, 3, 2, 5, 7, 6, 4)
    val root = createBinarySearchTree(test)
    val result = postOrder.postorderTraversal(root)
    val resultIterative = postOrder.traverseTreeIterative(root)
    if (result.toIntArray().contentEquals(expected)) println("Recursive OK")
    else println("Recursive: result is ${result.toIntArray().contentToString()} but expected is ${expected.contentToString()}")
    if (resultIterative.toIntArray().contentEquals(expected)) println("Iterative OK")
    else println("Iterative: result is ${resultIterative.toIntArray().contentToString()} but expected is ${expected.contentToString()}")

}