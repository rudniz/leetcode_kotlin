class SelfDividingNumber {
    fun selfDividingNumbers(left: Int, right: Int): List<Int> {
        val result = arrayListOf<Int>()
        for (i in left..right) {
            val digits = i.toString()
            var isDividing = true
            for (d in digits) {
                if (Character.getNumericValue(d) == 0 || i % Character.getNumericValue(d) != 0) {
                    isDividing = false
                    break
                }
            }
            if (isDividing) result.add(i)
            println("result is $result")
            println("i is $i")
        }
        return result
    }

    fun selfDividingNumbers2(left: Int, right: Int): List<Int> {
        val result = arrayListOf<Int>()
        for (i in left..right) {
            if (isValid(i)) result.add(i)
        }
        return result

    }

    fun isValid(number: Int): Boolean {
        var n = number
        while (n > 0) {
            if (n % 10 == 0 || number % (n % 10) != 0) return false
            n = n / 10
        }
        return true
    }
}

fun main(args: Array<String>) {
    val selfDividingNumber = SelfDividingNumber()
    val (a, b) = Pair(1, 22)
    val expected = intArrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 15, 22).asList()
    val result = selfDividingNumber.selfDividingNumbers2(a, b)
    if (result == expected) println("OK")
    else println("not ok")
}