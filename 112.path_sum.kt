import java.util.*

/**
 * Example:
 * var ti = TreeNode(5)
 * var v = ti.`val`
 * Definition for a binary tree node.
 * class TreeNode(var `val`: Int) {
 *     var left: TreeNode? = null
 *     var right: TreeNode? = null
 * }
 */
class PathSum {
    fun hasPathSum(root: TreeNode?, targetSum: Int): Boolean {
        if (root == null) return false
        if (root.right == null && root.left == null && root.`val` == targetSum) return true
        return hasPathSum(root.left, targetSum - root.`val`) || hasPathSum(root.right, targetSum - root.`val`)
    }

    fun hasPathSumIterative(root: TreeNode?, targetSum: Int): Boolean {
        if (root == null) return false
        val treeNodeStack = Stack<TreeNode>()
        val sumStack = Stack<Int>()
        treeNodeStack.push(root)
        sumStack.push(0)
        while (treeNodeStack.isNotEmpty()) {
            val currentNode = treeNodeStack.pop()
            val currentSum = sumStack.pop() + currentNode.`val`
            if (currentNode.left == null && currentNode.right == null && currentSum == targetSum) {
                return true
            }
            currentNode.left?.let {
                treeNodeStack.push(it)
                sumStack.push(currentSum)
            }
            currentNode.right?.let {
                treeNodeStack.push(currentNode.right)
                sumStack.push(currentSum)
            }
        }
        return false
    }

    fun makeTree(array: Array<Int?>): TreeNode? {
        if (array.isEmpty()) return null
        if (array[0] != null) {
            val root = TreeNode(array[0]!!)
            val treeNodes = LinkedList<TreeNode>()
            treeNodes.add(root)
            var i = 1
            while (!treeNodes.isEmpty()) {
                val currentNode = treeNodes.poll()
                if (i < array.size) {
                    array[i]?.let { currentNode.left = TreeNode(it) }
                }
                i++
                if (i < array.size) {
                    array[i]?.let { currentNode.right = TreeNode(it) }
                }
                i++
                currentNode.left?.let { treeNodes.add(it) }
                currentNode.right?.let { treeNodes.add(it) }
            }
            return root
        }
        return null
    }
}

fun main(args: Array<String>) {
    val numArray = arrayOf(5, 4, 8, 11, null, 13, 4, 7, 2, null, null, null, 1)
    val pathSum = PathSum()
    val root = pathSum.makeTree(numArray)
    val test = arrayOf(
        Pair(22, true),
        Pair(44, false)
    )
    for (pair in test) {
        val result = pathSum.hasPathSum(root, pair.first)
        if (result != pair.second) println("recursive result is $result but expected is ${pair.second}")
        else println("recursive result is OK")
    }
    for (pair in test) {
        val result = pathSum.hasPathSumIterative(root, pair.first)
        if (result != pair.second) println("iterative result is $result but expected is ${pair.second}")
        else println("iterative result is OK")
    }
}

