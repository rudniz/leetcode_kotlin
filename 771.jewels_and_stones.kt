class Jewels {
    fun numJewelsInStones(jewels: String, stones: String): Int {
        var result = 0
        val array = IntArray(256)
        for (ch in stones) {
            array[ch.toInt() - 'A'.toInt()]++
        }
        for (ch in jewels) {
            result += array[ch.toInt() - 'A'.toInt()]
        }
        return result
    }
}


fun main(args: Array<String>) {
    val jewels = Jewels()
    val tests = arrayOf(
        Pair("aA", "aAAbbbb"),
        Pair("z", "ZZ")
    )
    val expected = arrayOf(3, 0)
    for (i in tests.indices) {
        val result = jewels.numJewelsInStones(tests[i].first, tests[i].second)
        if (result != expected[i]) println("result is $result but expected is ${expected[i]}")
        else println("OK")
    }
}