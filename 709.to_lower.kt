class ToLower {
    fun toLowerCase(str: String): String {
        val lower = StringBuilder(str.length)
        for (i in 0 until str.length) {
            if (str[i].toInt() in 65..90) {
                lower.append(str[i] + 32)
            } else lower.append(str[i])
        }
        return lower.toString()
    }
}

fun main(args: Array<String>) {
    val toLower = ToLower()
    val tests = arrayOf("aDsA", "w@W4", "aa1A)")
    val expected = arrayOf("adsa", "w@w4", "aa1a)")
    for (i in 0 until tests.size) {
        val result = toLower.toLowerCase(tests[i])
        if (result != expected[i]) {
            println("result is $result but expected is ${expected[i]}")
        } else println("OK")
    }
}