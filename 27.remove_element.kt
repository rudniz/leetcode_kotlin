class Remove {
    fun removeElement(nums: IntArray, `val`: Int): IntArray {
        var newTail = 0
        for (j in 0 until nums.size) {
            if (nums[j] != `val`) {
                nums[newTail] = nums[j]
                newTail++
            }
        }
        return nums.slice(0..newTail - 1).toIntArray()
    }
}

fun main(args: Array<String>) {
    val remove = Remove()
    val test = arrayOf(
        intArrayOf(0, 1, 1, 2, 3, 1),
        intArrayOf(1, 2, 3),
        intArrayOf(1, 1, 1, 1),
        intArrayOf()
    )
    val valueToRemove = intArrayOf(1, 1, 1, 1)
    val expected = arrayOf(
        intArrayOf(0, 2, 3),
        intArrayOf(2, 3),
        intArrayOf(),
        intArrayOf()
    )
    for (i in 0 until test.size) {
        val result = remove.removeElement(test[i], valueToRemove[i])
        if (!result.contentEquals(expected[i])) {
            println("result is ${result.joinToString()} but expected is ${expected[i].joinToString()}")
        } else println("OK")
    }
}