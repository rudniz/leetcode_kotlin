class LongestUncommonSubSequence {
    fun findLUSlength(a: String, b: String): Int {
        if (a == b) return -1
        return a.length.coerceAtLeast(b.length)
    }
}

fun main() {
    val subsequence = LongestUncommonSubSequence()
    val tests = arrayOf(
        Pair("aba", "cdc"),
        Pair("aaa", "caca"),
        Pair("aaa", "aaa")
    )

    val expected = intArrayOf(3, 4, -1)

    for (i in tests.indices) {
        val result = subsequence.findLUSlength(tests[i].first, tests[i].second)
        if (result != expected[i]) println("result is $result but expected is ${expected[i]}")
        else println("OK")
    }
}