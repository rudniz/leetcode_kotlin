class BinarySearch {
    fun search(nums: IntArray, target: Int): Int {
        var start = 0
        var end = nums.size - 1
        while (start <= end) {
            val mid = (start + end) / 2
            if (nums[mid] == target) return mid
            if (nums[mid] > target) end = mid - 1
            else start = mid + 1
        }
        return -1
    }
}

fun main(args: Array<String>) {
    val binary = BinarySearch()
    val tests = intArrayOf(-10, -9, 0, 1, 2)
    val target = intArrayOf(-11, -10, -9, 3, 4, 1)
    val expected = intArrayOf(-1, 0, 1, -1, -1, 3)
    for (i in 0 until target.size) {
        val result = binary.search(tests, target[i])
        if (result != expected[i]) println("result is $result but expected is ${expected[i]}")
        else println("OK")
    }

}