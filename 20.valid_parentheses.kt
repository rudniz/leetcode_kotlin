class Parentheses {
    fun isValid(s: String): Boolean {
        val stack = mutableListOf<Char>()
        for (i in s) {
            when (i) {
                '(' -> stack.add(')')
                '{' -> stack.add('}')
                '[' -> stack.add(']')
                else -> if (stack.isEmpty() || stack[stack.size - 1] != i) {
                    return false
                } else stack.removeAt(stack.size - 1)
            }
        }
        return stack.isEmpty()
    }
}

fun main(args: Array<String>) {
    val parenteses = Parentheses()
    val tests = arrayOf("{}", "()[]{}", "[{[}", "{[(])]}", "{{")
    val expected = arrayOf(true, true, false, false, false)
    for (i in 0 until tests.size) {
        val result = parenteses.isValid(tests[i])
        if (result != expected[i]) {
            println("result is $result but expected is ${expected[i]}")
        } else println("OK")
    }
}