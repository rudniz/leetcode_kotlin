import kotlin.math.min

class ClimbStairs {

    private lateinit var table: IntArray

    fun minCostClimbingStairs(cost: IntArray): Int {
        var first = cost[0]
        var second = cost[1]
        var curr = 0
        for (i in 2 until cost.size) {
            curr = min(cost[i] + first, cost[i] + second)
            first = second
            second = curr
        }
        return min(curr, first)
    }

    fun minCostClimbingStairsRecursion(cost: IntArray): Int {
        table = IntArray(cost.size) { 0 }
        return min(backtrack(cost, cost.size - 1), backtrack(cost, cost.size - 2))
    }

    private fun backtrack(arr: IntArray, index: Int): Int {
        if (index < 0) return 0
        if (table[index] != 0) return table[index]
        table[index] = arr[index] + min(backtrack(arr, index - 1), backtrack(arr, index - 2))
        return table[index]
    }

    fun minCostClimbingStairs2(cost: IntArray): Int {
        val temp = cost.toMutableList()
        temp.add(0)
        for (i in 2 until temp.size) {
            temp[i] = min(temp[i - 1] + temp[i], temp[i - 2] + temp[i])
        }
        return temp[temp.size - 1]
    }
}

fun main(args: Array<String>) {
    val climb = ClimbStairs()
    val tests = arrayOf(
        intArrayOf(10, 15, 20),
        intArrayOf(1, 100, 1, 1, 1, 100, 1, 1, 100, 1),
        intArrayOf(0, 1)
    )
    val expected = intArrayOf(15, 6, 0)
    for (i in 0 until tests.size) {
        val result = climb.minCostClimbingStairsRecursion(tests[i])
        if (result != expected[i]) println("result is $result but expected is ${expected[i]}")
        else println("OK")
    }
}