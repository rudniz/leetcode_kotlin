class TwoSum {
    fun twoSum(nums: IntArray, target: Int): IntArray {
        val dictionary = HashMap<Int, Int>()
        for (i in 0..nums.size) {
            if (nums[i] in dictionary) {
                return intArrayOf(dictionary[nums[i]]!!, i)
            } else {
                dictionary[target - nums[i]] = i
            }
        }
        return intArrayOf()
    }
}

fun main(args: Array<String>) {
    val twoSum = TwoSum()
    val tests = arrayOf(
        intArrayOf(2, 7, 11, 15),
        intArrayOf(2, 7, 11, 15),
        intArrayOf(3, 2, 4)
    )
    val targets = intArrayOf(9, 17, 6)
    val expected = arrayOf(
        intArrayOf(0, 1),
        intArrayOf(0, 3),
        intArrayOf(1, 2)
    )
    for (i in 0 until tests.size) {
        val result = twoSum.twoSum(tests[i], targets[i])
        if (!result.contentEquals(expected[i])) {
            println("result is ${result.contentToString()} but expected is ${expected.contentToString()}")
        } else println("OK")
    }
}