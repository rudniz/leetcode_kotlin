class Climb {
    private lateinit var memo: IntArray
    fun climbStairs(n: Int): Int {
        if (n == 1) return 1
        if (n == 2) return 2
        var result = 0
        var first = 1
        var second = 2
        for (i in 3..n) {
            result = first + second
            first = second
            second = result
        }
        return result
    }

    fun climbStairsRecursive(n: Int): Int {
        if (n == 1) return 1
        if (n == 2) return 2
        return climbStairsRecursive(n - 1) + climbStairsRecursive(n - 2)
    }

    fun climbStairsRecursiveMemoTable(n: Int): Int {
        if (n == 1) return 1
        if (n == 2) return 2
        memo = IntArray(n) { 0 }
        memo[0] = 1
        memo[1] = 2
        return backtrack(n - 1)
    }

    fun backtrack(n: Int): Int {
        if (memo[n] != 0) return memo[n]
        memo[n] = backtrack(n - 1) + backtrack(n - 2)
        return memo[n]
    }
}

fun main(args: Array<String>) {
    val climb = Climb()
    val tests = intArrayOf(1, 2, 3, 4)
    val expected = intArrayOf(1, 2, 3, 5)
    for (i in 0 until tests.size) {
        val result = climb.climbStairsRecursiveMemoTable(tests[i])
        if (result != expected[i]) println("result is $result but expected is ${expected[i]}")
        else println("OK")
    }
}