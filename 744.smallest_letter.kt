class SmallestLetter {
    fun nextGreatestLetter(letters: CharArray, target: Char): Char {
        val presentLetters = IntArray(26) { 0 }
        for (i in letters) presentLetters[i - 'a'] = 1
        var charTarget = target
        while (true) {
            charTarget += 1
            if (charTarget > 'z') charTarget = 'a'
            if (presentLetters[charTarget - 'a'] == 1) return charTarget
        }
    }
}

fun main(args: Array<String>) {
    val smallestLetter = SmallestLetter()
    val letters = arrayOf(
        charArrayOf('c', 'f', 'j', 'a', 'c'),
        charArrayOf('c', 'f', 'j'),
        charArrayOf('c', 'f', 'j'),
        charArrayOf('c', 'f', 'j', 'a'),
        charArrayOf('c', 'g', 'f', 'j', 'a')
    )
    val target = arrayOf('a', 'c', 'j', 'a', 'f')
    val expected = arrayOf('c', 'f', 'c', 'c', 'g')
    for (i in 0 until letters.size) {
        val result = smallestLetter.nextGreatestLetter(letters[i], target[i])
        if (result != expected[i]) println("result is $result but expected is ${expected[i]}")
        else println("OK")
    }
}