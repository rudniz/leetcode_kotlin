class FloodFill {
    fun floodFill(image: Array<IntArray>, sr: Int, sc: Int, newColor: Int): Array<IntArray> {
        val color = image[sr][sc]
        dfs(image, sr, sc, newColor, color)
        return image
    }

    fun dfs(image: Array<IntArray>, sr: Int, sc: Int, newColor: Int, color: Int) {
        if (image[sr][sc] == color) {
            image[sr][sc] = newColor
            if (sr + 1 < image.size) {
                dfs(image, sr + 1, sc, newColor, color)
            }
            if (sr - 1 >= 0) {
                dfs(image, sr - 1, sc, newColor, color)
            }
            if (sc + 1 < image[0].size) {
                dfs(image, sr, sc + 1, newColor, color)
            }
            if (sc - 1 >= 0) {
                dfs(image, sr, sc - 1, newColor, color)
            }
        }
    }
}

fun main(args: Array<String>) {
    val floodFill = FloodFill()
    val tests = arrayOf(
        intArrayOf(1, 1, 1),
        intArrayOf(1, 1, 0),
        intArrayOf(1, 0, 1)
    )
    val expected = arrayOf(intArrayOf(2, 2, 2), intArrayOf(2, 2, 0), intArrayOf(2, 0, 1))
    val result = floodFill.floodFill(tests, 1, 1, 2)
    if (!result.contentDeepEquals(expected)) println("result is ${result.contentDeepToString()} but expected is ${expected.contentDeepToString()}")
    else println("OK")
}