class Length_Of_Last_Word {
    fun lengthOfLastWord(s: String): Int {
        var curr = s.length - 1
        var length = 0
        while (curr >= 0 && s[curr] == ' ') {
            curr--
        }
        while (curr >= 0 && s[curr] != ' ') {
            curr--
            length++
        }
        return length
    }
}

fun main(args: Array<String>) {
    val wordLength = Length_Of_Last_Word()
    val tests = arrayOf("Jjj jjf jj", "", " ", "jjj", " j", "a ")
    val expected = intArrayOf(2, 0, 0, 3, 1, 1)
    for (i in 0 until tests.size) {
        val result = wordLength.lengthOfLastWord(tests[i])
        if (result != expected[i]) {
            println("result is $result but expected is ${expected[i]}")
        } else println("OK")
    }
}