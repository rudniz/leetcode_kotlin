class Merge {
    fun mergeTwoLists(l1: ListNode?, l2: ListNode?): ListNode? {
        if (l1 == null) return l2
        if (l2 == null) return l1
        val head = ListNode(0)
        var current = head
        var list1 = l1
        var list2 = l2
        while (list1 != null && list2 != null) {
            if (list1.`val` < list2.`val`) {
                current.next = list1
                list1 = list1.next
            } else {
                current.next = list2
                list2 = list2.next
            }
            current = current.next!!
        }
        if (list1 != null) {
            current.next = list1
        } else if (list2 != null) {
            current.next = list2
        }
        return head.next
    }

    fun mergeTwoListsRecursively(l1: ListNode?, l2: ListNode?): ListNode? {
        if (l1 == null) return l2
        if (l2 == null) return l1
        if (l1.`val` < l2.`val`) {
            l1.next = mergeTwoListsRecursively(l1.next, l2)
            return l1
        } else {
            l2.next = mergeTwoListsRecursively(l1, l2.next)
            return l2
        }
    }


    fun arrayToLinkedList(arr: IntArray): ListNode? {
        val head = if (arr.size > 0) ListNode(arr[0]) else null
        var traverse = head
        for (i in 1 until arr.size) {
            traverse?.next = ListNode(arr[i])
            traverse = traverse?.next
        }
        return head
    }

    fun linkedListToString(l: ListNode?): String {
        val arr = ArrayList<Int>()
        var traverse = l
        while (traverse != null) {
            arr.add(traverse.`val`)
            traverse.let { traverse = traverse?.next }
        }
        return arr.joinToString()
    }
}

fun main(args: Array<String>) {
    val merge = Merge()
    val list1 = arrayOf(
        intArrayOf(1, 2, 4),
        intArrayOf(4, 7),
        intArrayOf(1, 2, 4, 4, 6),
        intArrayOf(1, 2),
        intArrayOf(),
        intArrayOf(1)
    )

    val list2 = arrayOf(
        intArrayOf(1, 5, 6),
        intArrayOf(1, 2, 9),
        intArrayOf(1, 2, 3, 5, 7, 8, 9),
        intArrayOf(3, 4),
        intArrayOf(),
        intArrayOf()
    )
    val expected = arrayOf(
        intArrayOf(1, 1, 2, 4, 5, 6),
        intArrayOf(1, 2, 4, 7, 9),
        intArrayOf(1, 1, 2, 2, 3, 4, 4, 5, 6, 7, 8, 9),
        intArrayOf(1, 2, 3, 4),
        intArrayOf(),
        intArrayOf(1)
    )
    for (i in 0 until list1.size) {
        val resultLinkedList =
            merge.mergeTwoListsRecursively(merge.arrayToLinkedList(list1[i]), merge.arrayToLinkedList(list2[i]))
        val result = merge.linkedListToString(resultLinkedList)
        if (result == expected[i].joinToString()) {
            println("OK")
        } else {
            println("result is $result but expected is ${expected[i].joinToString()}")
        }

    }

}