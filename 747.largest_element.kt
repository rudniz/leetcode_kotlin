import java.lang.Integer.max

class LargestElement {
    fun dominantIndex(nums: IntArray): Int {
        val largestElement = nums.max()
        for (i in 0 until nums.size) {
            if (i * 2 > largestElement!!) return -1
        }
        return nums.indexOf(largestElement!!)
    }

    fun dominantIndex2(nums: IntArray): Int {
        var largest = -1
        var secondLargest = -1
        var index = -1
        for (i in 0 until nums.size) {
            if (nums[i] > secondLargest) {
                secondLargest = nums[i]

            }
            if (nums[i] > largest){
                secondLargest = largest.also { largest = secondLargest }
                index = i
            }
        }
        if (secondLargest * 2 > largest) return -1
        return index
    }
}

fun main(args: Array<String>) {
    val largestElement = LargestElement()
    val tests = arrayOf(
        intArrayOf(1, 2, 3, 4),
        intArrayOf(6, 1, 3, 2),
        intArrayOf(0),
        intArrayOf(2, 2, 2)
    )

    val expected = arrayOf(-1, 0, 0, -1)
    for (i in 0 until tests.size) {
        val result = largestElement.dominantIndex2(tests[i])
        if (result != expected[i]) {
            println("result is $result but expected is ${expected[i]}")
        } else println("OK")
    }
}