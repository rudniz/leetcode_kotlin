class RelativeRank {
    fun findRelativeRanks(score: IntArray): Array<String> {
        val scoreWithIndex = score
            .mapIndexed { idx, it -> Pair(idx, it) }
            .sortedByDescending { it.second }

        val result = Array(scoreWithIndex.size) { "" }
        for (i in scoreWithIndex.indices) {
            val index = scoreWithIndex[i].first
            when (i) {
                0 -> result[index] = "Gold Medal"
                1 -> result[index] = "Silver Medal"
                2 -> result[index] = "Bronze Medal"
                else -> result[index] = (i + 1).toString()
            }
        }
        return result
    }

    fun findRelativeRanksWithMap(score: IntArray): Array<String> {
        val map = score.toList()
            .associateWith { e -> score.indexOf(e) }
            .toSortedMap(reverseOrder())
        val result = Array(map.size) { "" }
        for ((i, k) in map.keys.withIndex()) {
            val index = map[k]!!
            when (i) {
                0 -> result[index] = "Gold Medal"
                1 -> result[index] = "Silver Medal"
                2 -> result[index] = "Bronze Medal"
                else -> result[index] = (i + 1).toString()
            }
        }
        return result
    }
}

fun main() {
    val relativeRank = RelativeRank()
    val test = arrayOf(
        intArrayOf(5, 4, 3, 2, 1),
        intArrayOf(10, 3, 8, 9, 4)
    )
    val expected = arrayOf(
        arrayOf("Gold Medal", "Silver Medal", "Bronze Medal", "4", "5"),
        arrayOf("Gold Medal", "5", "Bronze Medal", "Silver Medal", "4")
    )
    for (i in test.indices) {
        val result = relativeRank.findRelativeRanks(test[i])
        if (!result.contentEquals(expected[i])) println(
            "result is ${result.contentToString()} " +
                    "but expected is ${expected[i].contentToString()}"
        )
        else println("OK")
    }
    println()

    for (i in test.indices) {
        val result = relativeRank.findRelativeRanksWithMap(test[i])
        if (!result.contentEquals(expected[i])) println(
            "result is ${result.contentToString()} " +
                    "but expected is ${expected[i].contentToString()}"
        )
        else println("OK")
    }
}