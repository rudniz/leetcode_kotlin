class Shortest {
    fun shortestCompletingWord(licensePlate: String, words: Array<String>): String {
        val letters = licensePlate.toLowerCase().filter { it.isLetter() }
        val map = letters.associate { it to countOccurrences(letters, it) }
        var shortest = ""
        for (word in words) {
            if (containsAllLetters(word, map)) {
                if (shortest.isEmpty() || shortest.length > word.length) shortest = word
            }
        }
        return shortest
    }

    private fun containsAllLetters(word: String, map: Map<Char, Int>): Boolean {
        for (key in map.keys) {
            if (map[key]!! > countOccurrences(word, key)) {
                return false
            }
        }
        return true
    }

    private fun countOccurrences(s: String, ch: Char): Int {
        return s.filter { it == ch }.count()
    }

    fun shortestCompletingWordArray(licensePlate: String, words: Array<String>): String {
        val count = IntArray(26) { 0 }
        val filtered = licensePlate.toLowerCase().filter { it.isLetter() }
        for (i in filtered) {
            count[i - 'a']++
        }
        var shortest = ""
        for (word in words) {
            val temp = count.copyOf()
            for (letter in word) {
                temp[letter - 'a']--
            }
            var flag = true
            for (i in temp) {
                if (i > 0) {
                    flag = false
                    break
                }
            }
            if (flag && (shortest.isEmpty() || shortest.length > word.length)) {
                shortest = word
            }
        }
        return shortest
    }
}

fun main(args: Array<String>) {
    val shortest = Shortest()
    val testWords = arrayOf("1s3 PSt", "1s3 456", "Ah71752", "OgEu755", "iMSlpe4")

    val testArrays = arrayOf(
        arrayOf("step", "steps", "stripe", "stepple"),
        arrayOf("looks", "pest", "stew", "show"),
        arrayOf("suggest", "letter", "of", "husband", "easy", "education", "drug", "prevent", "writer", "old"),
        arrayOf("enough", "these", "play", "wide", "wonder", "box", "arrive", "money", "tax", "thus"),
        arrayOf("claim", "consumer", "student", "camera", "public", "never", "wonder", "simple", "thought", "use")
    )

    val expected = arrayOf("steps", "pest", "husband", "enough", "simple")
    for (i in 0 until testWords.size) {
        val resultMap = shortest.shortestCompletingWord(testWords[i], testArrays[i])
        val result = shortest.shortestCompletingWordArray(testWords[i], testArrays[i])
        if (resultMap != expected[i]) {
            println("result using Map is $resultMap but expected is ${expected[i]}")
        } else println("result map OK")
        if (result != expected[i]) {
            println("result using array is $result but expected is ${expected[i]}")
        } else println("result array OK")
    }
}