import java.util.*
import kotlin.collections.ArrayList

/**
 * Example:
 * var ti = TreeNode(5)
 * var v = ti.`val`
 * Definition for a binary tree node.
 * class TreeNode(var `val`: Int) {
 *     var left: TreeNode? = null
 *     var right: TreeNode? = null
 * }
 */
class PreorderTraversalBST {
    val result = arrayListOf<Int>()

    fun preorderTraversal(root: TreeNode?): List<Int> {
        traverseTree(root)
        return result
    }

    private fun traverseTree(root: TreeNode?) {
        if (root == null) return
        result.add(root.`val`)
        traverseTree(root.left)
        traverseTree(root.right)
    }

    fun preorderTraversalIterative(root: TreeNode?): List<Int> {
        val resultIterative = ArrayList<Int>()
        if (root == null) return result
        val stack = ArrayDeque<TreeNode>()
        stack.push(root)
        while (!stack.isEmpty()) {
            val curr = stack.pop()
            resultIterative.add(curr.`val`)
            if (curr.right != null) {
                stack.push(curr.right)
            }
            if (curr.left != null) {
                stack.push(curr.left)
            }
        }
        return resultIterative
    }
}

fun createBST(list: List<Int>): TreeNode? {
    if (list.isEmpty()) return null
    val end = list.size
    val mid = end / 2
    val root = TreeNode(list[mid])
    root.right = createBST(list.subList(mid + 1, end))
    root.left = createBST(list.subList(0, mid))
    return root
}

fun main() {
    val preorderTraversal = PreorderTraversalBST()
    val test = listOf(1, 2, 3, 4, 5, 6, 7)
    val expected = intArrayOf(4, 2, 1, 3, 6, 5, 7)
    val root = createBST(test)
    val result = preorderTraversal.preorderTraversal(root)
    val resultIterative = preorderTraversal.preorderTraversalIterative(root)
    if (expected.contentEquals(result.toIntArray())) println("Recursive OK")
    else {
        println(
            "Recursive: expected is ${expected.contentToString()} " +
                    "but the result is ${result.toIntArray().contentToString()}"
        )
    }
    if (expected.contentEquals(resultIterative.toIntArray())) println("Iterative OK")
    else {
        println(
            "Iterative: expected is ${expected.contentToString()} " +
                    "but the result is ${resultIterative.toIntArray().contentToString()}"
        )
    }

}