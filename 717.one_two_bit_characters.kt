class OneBitCharacter {
    fun isOneBitCharacter(bits: IntArray): Boolean {
        var i = 0
        while (i < bits.size) {
            if (bits[i] == 1) {
                if (i < bits.size - 1) i += 2
                else return false
            } else {
                if (i < bits.size - 1) i += 1
                else return bits[i] == 0
            }
        }
        return false
    }

    fun isOneBitCharacter2(bits: IntArray): Boolean {
        if (bits.size == 1) return true
        var curr = bits.size - 2
        var count = 0
        while (curr >= 0 && bits[curr] == 1) {
            count += 1
            curr -= 1
        }
        return count % 2 == 0
    }
}

fun main(args: Array<String>) {
    val oneBitCharacter = OneBitCharacter()
    val tests = arrayOf(
        intArrayOf(1, 0, 0),
        intArrayOf(1, 1, 1, 0),
        intArrayOf(0),
        intArrayOf(1, 1),
        intArrayOf(1, 0)
    )
    val expected = arrayOf(true, false, true, false, false)
    for (i in 0 until tests.size) {
        val result = oneBitCharacter.isOneBitCharacter2(tests[i])
        if (result != expected[i]) println("result is $result but expected is ${expected[i]}")
        else println("OK")
    }
}