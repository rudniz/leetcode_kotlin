/**
 * Example:
 * var ti = TreeNode(5)
 * var v = ti.`val`
 * Definition for a binary tree node.
 * class TreeNode(var `val`: Int) {
 *     var left: TreeNode? = null
 *     var right: TreeNode? = null
 * }
 */
class FindModeBst {
    var maxCount = 0
    var prev = 0
    var numberOfDuplicates = 0
    var result = arrayListOf<Int>()

    fun findMode(root: TreeNode?): IntArray {
        traverse(root)
        return result.toIntArray()
    }

    fun traverse(root: TreeNode?) {
        if (root == null) return
        traverse(root.left)
        numberOfDuplicates = if(root.`val` == prev) numberOfDuplicates + 1 else 1
        if (numberOfDuplicates == maxCount) result.add(root.`val`)
        if (numberOfDuplicates > maxCount) {
            result = arrayListOf(root.`val`)
            maxCount = numberOfDuplicates
        }
        prev = root.`val`
        traverse(root.right)
    }

    fun createBST(): TreeNode? {
        val root = TreeNode(1)
        val next = TreeNode(2)
        next.left = TreeNode(3)
        next.right = TreeNode(2)
        root.left = next
        return root
    }
}

fun main(args: Array<String>) {

    val expected = intArrayOf(2)
    val modes = FindModeBst()
    val tree = modes.createBST()
    val result = modes.findMode(tree)
    if (result.contentEquals(expected)) {
        println("OK")
    } else {
        for (i in 0 until result.size) {
            println("result is ${result[i]} but expected is ${expected[i]}")
        }
    }
}