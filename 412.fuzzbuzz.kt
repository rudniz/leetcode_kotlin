class Solution {
    fun fizzBuzz(n: Int): List<String> {
        val res = ArrayList<String>()
        for (i in 1..n) {
            if (i % 3 == 0) {
                if (i % 5 == 0) {
                    res.add("FizzBuzz")
                } else res.add("Fizz")
            } else {
                if (i % 5 == 0) {
                    res.add("Buzz")
                } else res.add(i.toString())
            }
        }
        return res
    }
}

fun main(args: Array<String>) {
    val s = Solution()
    println(s.fizzBuzz(15))
}
