import java.lang.Integer.MAX_VALUE
import java.lang.Integer.min
import java.lang.Math.abs


/**
 * Example:
 * var ti = TreeNode(5)
 * var v = ti.`val`
 * Definition for a binary tree node.
 * class TreeNode(var `val`: Int) {
 *     var left: TreeNode? = null
 *     var right: TreeNode? = null
 * }
 */
class MinimumDifference {
    var minimumDifference = MAX_VALUE
    var prev = MAX_VALUE
    fun getMinimumDifference(root: TreeNode?): Int {
        if (root == null) return -1
        traverseTree(root)
        return minimumDifference

    }

    fun traverseTree(root: TreeNode?) {
        if (root == null) return
        traverseTree(root.left)
        minimumDifference = min(minimumDifference, abs(root.`val` - prev))
        prev = root.`val`
        traverseTree(root.right)
    }
}


fun arrayToTree(array: List<Int>): TreeNode? {
    if (array.isEmpty()) return null
    if (array.size == 1) return TreeNode(array[0])
    val mid = array.size / 2
    val root = TreeNode(array[mid])
    root.left = arrayToTree(array.slice(0 until mid))
    root.right = arrayToTree(array.slice(mid + 1 until array.size))
    return root
}


fun main(args: Array<String>) {
    val test = arrayOf(
        intArrayOf(4, 2, 6, 1, 3),
        intArrayOf(4, 2, 6, 8, 11),
        intArrayOf(4, 1, 7, 12, 33),
        intArrayOf(236, 104, 701, 227, 911)
    )
    val expected = intArrayOf(1, 2, 3, 9)

    for (i in test.indices) {
        val difference = MinimumDifference()
        val tree = arrayToTree(test[i].sorted())
        val result = difference.getMinimumDifference(tree)
        if (result != expected[i]) println("result is $result but expected is ${expected[i]}")
        else println("OK")
    }
}


