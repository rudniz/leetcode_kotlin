import kotlin.math.sqrt

class PerfectNumber {
    fun checkPerfectNumber(num: Int): Boolean {
        if (num == 1) return false
        var result = 1
        val root = sqrt(num.toDouble()).toInt()
        for (i in 2..root) {
            if (num % i == 0) {
                result += i + num / i
            }
        }
        return result == num
    }
}

fun main() {
    val perfectNumber = PerfectNumber()
    val tests = intArrayOf(28, 6, 496, 8128, 2)
    val expected = arrayOf(true, true, true, true, false)
    for (i in tests.indices) {
        val result = perfectNumber.checkPerfectNumber(tests[i])
        if (result != expected[i]) println("result is $result but expected is ${expected[i]}")
        else println("OK")
    }
}