class PalindromeNumber {
    fun isPalindrome(x: Int): Boolean {
        return x == reverse(x)
    }

    fun reverse(x: Int): Int {
        var result = 0
        var original = x
        while (original > 0) {
            result = result * 10 + original % 10
            original = original / 10
        }
        return result
    }

    fun isPalindrome2(x: Int): Boolean {
        if (x < 0 || (x != 0 && x % 10 == 0)) return false
        var result = 0
        var original = x
        while (original > result) {
            result = result * 10 + original % 10
            original = original / 10
        }
        return (result == original || result / 10 == original)
    }
}

fun main(args: Array<String>) {
    val palindromeNumber = PalindromeNumber()
    val tests = intArrayOf(121, 12321, -121, 0, 123421)
    val expected = arrayOf(true, true, false, true, false)
    for (i in 0 until tests.size - 1) {
        val result = palindromeNumber.isPalindrome2(tests[i])
        if (result != expected[i]) {
            println("result is $result but expected is ${expected[i]}")
        } else println("OK")
    }
}